#include "Matrix.h"

Matrix Matrix::get8x8Template(char c) {
	Matrix matrix = Matrix(8, 8);
	if (c == '0' || c == 0) {
		matrix.setAll();
		matrix.set(0, 0);
		matrix.set(0, 7);
		matrix.setRange(2, 2, 1, 2);
		matrix.set(3, 2);
		matrix.set(4, 5);
		matrix.setRange(5, 4, 1, 2);
		matrix.set(7, 0);
		matrix.set(7, 7);
	} else if (c == '1' || c == 1) {
		matrix.setRange(0, 3, 6, 2);
		matrix.setRange(1, 1, 2, 2);
		matrix.setRange(6, 0, 2, 8);
	} else if (c == '2' || c == 2) {
		matrix.setRange(0, 0, 2, 6);
		matrix.setRange(1, 6, 3, 2);
		matrix.setRange(3, 2, 2, 4);
		matrix.setRange(4, 0, 2, 2);
		matrix.setRange(6, 0, 2, 8);
	} else if (c == '3' || c == 3) {
		matrix.setRange(1, 0, 2, 2);
		matrix.setRange(5, 0, 2, 2);
		matrix.setRange(1, 6, 6, 2);
		matrix.setRange(3, 3, 2, 3);
		matrix.setRange(0, 1, 2, 6, true);
		matrix.setRange(6, 1, 2, 6, true);
	} else if (c == '4' || c == 4) {
		matrix.setRange(0, 0, 3, 2);
		matrix.setRange(0, 3, 8, 2);
		matrix.setRange(3, 0, 2, 8, true);
	} else if (c == '5' || c == 5) {
		matrix.setRange(0, 0, 2, 8);
		matrix.setRange(2, 0, 3, 2);
		matrix.setRange(3, 2, 2, 4);
		matrix.setRange(4, 6, 3, 2);
		matrix.setRange(6, 0, 2, 6);
	} else if (c == '6' || c == 6) {
		matrix.setRange(0, 1, 2, 7);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(1, 0, 6, 2, true);
		matrix.setRange(4, 6, 3, 2, true);
	} else if (c == '7' || c == 7) {
		matrix.setRange(0, 0, 2, 8);
		matrix.setRange(2, 0, 1, 2);
		matrix.setRange(2, 5, 1, 3);
		matrix.setRange(3, 4, 1, 3);
		matrix.setRange(4, 3, 1, 3);
		matrix.setRange(5, 3, 3, 2);
	} else if (c == '8' || c == 8) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(1, 0, 6, 2, true);
		matrix.setRange(1, 6, 6, 2, true);
	} else if (c == '9' || c == 9) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(6, 0, 2, 7);
		matrix.setRange(1, 0, 3, 2, true);
		matrix.setRange(1, 6, 6, 2, true);
	} else if (c == 'A' || c == 'a' || c == 10) {
		matrix.setRange(0, 3, 1, 2);
		matrix.setRange(1, 2, 1, 4);
		matrix.setRange(2, 1, 4, 6);
		matrix.setRange(3, 3, 1, 2);
		matrix.setRange(5, 0, 3, 2, true);
		matrix.setRange(5, 6, 3, 2, true);
	} else if (c == 'B' || c == 'b' || c == 11) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(0, 0, 8, 2, true);
		matrix.setRange(1, 6, 6, 2, true);
	} else if (c == 'C' || c == 'c' || c == 12) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(1, 0, 6, 2, true);
		matrix.setRange(1, 6, 2, 2, true);
		matrix.setRange(5, 6, 2, 2, true);
	} else if (c == 'D' || c == 'd' || c == 13) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(0, 0, 8, 2, true);
		matrix.setRange(1, 6, 6, 2, true);
	} else if (c == 'E' || c == 'e' || c == 14) {
		matrix.setRange(0, 1, 2, 7);
		matrix.setRange(3, 1, 2, 5);
		matrix.setRange(6, 1, 2, 7);
		matrix.setRange(0, 0, 8, 2, true);
	} else if (c == 'F' || c == 'f' || c == 15) {
		matrix.setRange(0, 1, 2, 7);
		matrix.setRange(3, 1, 2, 5);
		matrix.setRange(0, 0, 8, 2, true);
	} else if (c == 'G' || c == 'g' || c == 16) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(1, 0, 6, 2, true);
		matrix.setRange(1, 6, 2, 2, true);
		matrix.setRange(4, 6, 3, 2, true);
		matrix.setRange(3, 3, 2, 3);
	} else if (c == 'H' || c == 'h' || c == 17) {
		matrix.setRange(0, 0, 8, 2);
		matrix.setRange(0, 6, 8, 2);
		matrix.setRange(3, 2, 2, 4);
	} else if (c == 'I' || c == 'i' || c == 18) {
		matrix.setRange(0, 0, 2, 8);
		matrix.setRange(6, 0, 2, 8);
		matrix.setRange(2, 3, 4, 2);
	} else if (c == 'J' || c == 'j' || c == 19) {
		matrix.setRange(0, 0, 2, 8);
		matrix.setRange(2, 3, 4, 2);
		matrix.setRange(6, 1, 2, 4);
		matrix.setRange(4, 0, 3, 2, true);
	} else if (c == 'K' || c == 'k' || c == 20) {
		matrix.setRange(0, 0, 8, 2);
		matrix.setRange(3, 2, 2, 1);
		matrix.setRange(2, 3, 4, 2);
		matrix.setRange(1, 4, 2, 2, true);
		matrix.setRange(5, 4, 2, 2, true);
		matrix.setRange(0, 5, 2, 3, true);
		matrix.setRange(6, 5, 2, 3, true);
	} else if (c == 'L' || c == 'l' || c == 21) {
		matrix.setRange(0, 0, 8, 2);
		matrix.setRange(6, 2, 2, 6);
	} else if (c == 'M' || c == 'm' || c == 22) {
		matrix.setRange(0, 0, 8, 2);
		matrix.setRange(0, 6, 8, 2);
		matrix.setRange(1, 2, 3, 1);
		matrix.setRange(1, 5, 3, 1);
		matrix.setRange(2, 3, 3, 2);
	} else if (c == 'N' || c == 'n' || c == 23) {
		matrix.setRange(0, 0, 8, 2);
		matrix.setRange(0, 6, 8, 2);
		matrix.setRange(1, 2, 3, 1);
		matrix.setRange(2, 3, 3, 1);
		matrix.setRange(3, 4, 3, 1);
		matrix.setRange(4, 5, 3, 1);
	} else if (c == 'O' || c == 'o' || c == 24) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(1, 0, 6, 2, true);
		matrix.setRange(1, 6, 6, 2, true);
	} else if (c == 'P' || c == 'p' || c == 25) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(0, 0, 8, 2, true);
		matrix.setRange(1, 6, 3, 2, true);
	} else if (c == 'Q' || c == 'q' || c == 26) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(6, 1, 2, 6);
		matrix.setRange(1, 0, 6, 2, true);
		matrix.setRange(1, 6, 6, 2, true);
		matrix.setRange(3, 3, 2, 2);
		matrix.setRange(4, 4, 2, 2, true);
	} else if (c == 'R' || c == 'r' || c == 27) {
		matrix.setRange(0, 1, 2, 6);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(0, 0, 8, 2, true);
		matrix.setRange(1, 6, 3, 2, true);
		matrix.setRange(5, 3, 1, 3);
		matrix.setRange(6, 4, 1, 4);
		matrix.setRange(7, 5, 1, 3);
	} else if (c == 'S' || c == 's' || c == 28) {
		matrix.setRange(0, 1, 2, 7);
		matrix.setRange(3, 1, 2, 6);
		matrix.setRange(6, 0, 2, 7);
		matrix.setRange(1, 0, 3, 2, true);
		matrix.setRange(4, 6, 3, 2, true);
	} else if (c == 'T' || c == 't' || c == 29) {
		matrix.setRange(0, 0, 2, 8);
		matrix.setRange(2, 3, 6, 2);
	} else if (c == 'U' || c == 'u' || c == 30) {
		matrix.setRange(0, 0, 7, 2);
		matrix.setRange(0, 6, 7, 2);
		matrix.setRange(6, 1, 2, 6, true);
	} else if (c == 'V' || c == 'v' || c == 31) {
		matrix.setRange(0, 0, 3, 2);
		matrix.setRange(0, 6, 3, 2);
		matrix.setRange(3, 1, 2, 2);
		matrix.setRange(3, 5, 2, 2);
		matrix.setRange(5, 2, 2, 4);
		matrix.setRange(7, 3, 1, 2);
	} else if (c == 'W' || c == 'w' || c == 32) {
		matrix.setRange(0, 0, 7, 2);
		matrix.setRange(0, 6, 7, 2);
		matrix.setRange(3, 3, 3, 2);
		matrix.setRange(6, 1, 2, 6, true);
		matrix.setRange(7, 3, 1, 2);
	} else if (c == 'X' || c == 'x' || c == 33) {
		matrix.setRange(0, 0, 2, 2);
		matrix.setRange(0, 6, 2, 2);
		matrix.setRange(6, 0, 2, 2);
		matrix.setRange(6, 6, 2, 2);
		matrix.setRange(1, 1, 2, 2, true);
		matrix.setRange(1, 5, 2, 2, true);
		matrix.setRange(5, 1, 2, 2, true);
		matrix.setRange(5, 5, 2, 2, true);
		matrix.setRange(2, 2, 4, 4, true);
	} else if (c == 'Y' || c == 'y' || c == 34) {
		matrix.setRange(0, 0, 2, 2);
		matrix.setRange(0, 6, 2, 2);
		matrix.setRange(1, 1, 2, 2, true);
		matrix.setRange(1, 5, 2, 2, true);
		matrix.setRange(2, 2, 2, 4, true);
		matrix.setRange(4, 3, 4, 2);
	} else if (c == 'Z' || c == 'z' || c == 35) {
		matrix.setRange(0, 0, 2, 8);
		matrix.setRange(2, 0, 1, 2);
		matrix.setRange(6, 0, 2, 8);
		matrix.setRange(5, 6, 1, 2);
		matrix.setRange(2, 4, 1, 3);
		matrix.setRange(3, 3, 1, 3);
		matrix.setRange(4, 2, 1, 3);
		matrix.setRange(5, 1, 1, 3);
	}
	return matrix;
}

Matrix Matrix::get7x5Template(char c) {
	Matrix matrix = Matrix(7, 5);
	if (c == '0' || c == 0) {
		matrix.setRange(0, 1, 1, 3);
		matrix.setRange(6, 1, 1, 3);
		matrix.setRange(1, 0, 5, 1);
		matrix.setRange(1, 4, 5, 1);
		matrix.set(2, 3);
		matrix.set(3, 2);
		matrix.set(4, 1);
	} else if (c == '1' || c == 1) {
		matrix.set(1, 1);
		matrix.setRange(0, 2, 6, 1);
		matrix.setRange(6, 0, 1, 5);
	} else if (c == '2' || c == 2) {
		matrix.set(1, 0);
		matrix.setRange(0, 1, 1, 3);
		matrix.set(1, 4);
		matrix.set(2, 3);
		matrix.set(3, 2);
		matrix.set(4, 1);
		matrix.set(5, 0);
		matrix.setRange(6, 0, 1, 5);
	} else if (c == '3' || c == 3) {
		matrix.set(1, 0);
		matrix.setRange(0, 1, 1, 3);
		matrix.setRange(1, 4, 2, 1);
		matrix.setRange(3, 2, 1, 2);
		matrix.setRange(4, 4, 2, 1);
		matrix.setRange(6, 1, 1, 3);
		matrix.set(5, 0);
	} else if (c == '4' || c == 4) {
		matrix.setRange(0, 0, 4, 1);
		matrix.setRange(0, 2, 7, 1);
		matrix.setRange(3, 0, 1, 5, true);
	} else if (c == '5' || c == 5) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(1, 0, 3, 1);
		matrix.setRange(3, 1, 1, 3);
		matrix.setRange(4, 4, 2, 1);
		matrix.setRange(6, 1, 1, 3);
		matrix.set(5, 0);
	} else if (c == '6' || c == 6) {
		matrix.setRange(0, 0, 1, 4);
		matrix.setRange(3, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(3, 4, 4, 1);
		matrix.set(1, 4);
	} else if (c == '7' || c == 7) {
		matrix.setRange(0, 0, 1, 5);
		matrix.set(1, 0);
		matrix.set(1, 4);
		matrix.set(2, 3);
		matrix.setRange(3, 2, 4, 1);
	} else if (c == '8' || c == 8) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(3, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
	} else if (c == '9' || c == 9) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(3, 0, 1, 5);
		matrix.setRange(6, 1, 1, 5);
		matrix.setRange(0, 0, 4, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.set(5, 0);
	} else if (c == 'A' || c == 'a' || c == 10) {
		matrix.set(0, 2);
		matrix.setRange(1, 1, 3, 3);
		matrix.setRange(4, 0, 3, 5);
		matrix.setRange(1, 2, 2, 1);
		matrix.setRange(4, 1, 3, 3);
	} else if (c == 'B' || c == 'b' || c == 11) {
		matrix.setRange(0, 1, 1, 4);
		matrix.setRange(3, 1, 1, 4);
		matrix.setRange(6, 1, 1, 4);
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
	} else if (c == 'C' || c == 'c' || c == 12) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.setRange(2, 4, 3, 1);
	} else if (c == 'D' || c == 'd' || c == 13) {
		matrix.setRange(0, 1, 1, 4);
		matrix.setRange(6, 1, 1, 4);
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
	} else if (c == 'E' || c == 'e' || c == 14) {
		matrix.setRange(0, 1, 1, 4);
		matrix.setRange(3, 1, 1, 2);
		matrix.setRange(6, 1, 1, 4);
		matrix.setRange(0, 0, 7, 1);
	} else if (c == 'F' || c == 'f' || c == 15) {
		matrix.setRange(0, 1, 1, 4);
		matrix.setRange(3, 1, 1, 2);
		matrix.setRange(0, 0, 7, 1);
	} else if (c == 'G' || c == 'g' || c == 16) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.setRange(3, 2, 1, 2);
		matrix.set(2, 4);
	} else if (c == 'H' || c == 'h' || c == 17) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.setRange(3, 1, 1, 3);
	} else if (c == 'I' || c == 'i' || c == 18) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
		matrix.setRange(1, 2, 5, 1);
	} else if (c == 'J' || c == 'j' || c == 19) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(1, 2, 5, 1);
		matrix.set(6, 1);
		matrix.set(5, 0);
	} else if (c == 'K' || c == 'k' || c == 20) {
		matrix.setRange(0, 0, 7, 1);
		matrix.set(3, 1);
		matrix.set(2, 2);
		matrix.set(4, 2);
		matrix.set(1, 3);
		matrix.set(5, 3);
		matrix.set(0, 4);
		matrix.set(6, 4);
	} else if (c == 'L' || c == 'l' || c == 21) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(6, 1, 1, 4);
	} else if (c == 'M' || c == 'm' || c == 22) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.set(1, 1);
		matrix.set(2, 2);
		matrix.set(1, 3);
	} else if (c == 'N' || c == 'n' || c == 23) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.set(2, 1);
		matrix.set(3, 2);
		matrix.set(4, 3);
	} else if (c == 'O' || c == 'o' || c == 24) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
	} else if (c == 'P' || c == 'p' || c == 25) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 4, 1);
		matrix.setRange(0, 1, 1, 4);
		matrix.setRange(3, 1, 1, 4);
	} else if (c == 'Q' || c == 'q' || c == 26) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 7, 1);
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(6, 0, 1, 5);
		matrix.set(3, 2);
		matrix.set(4, 3);
	} else if (c == 'R' || c == 'r' || c == 27) {
		matrix.setRange(0, 0, 7, 1);
		matrix.setRange(0, 4, 4, 1);
		matrix.setRange(0, 1, 1, 4);
		matrix.setRange(3, 1, 1, 4);
		matrix.set(4, 2);
		matrix.set(5, 3);
		matrix.set(6, 4);
	} else if (c == 'S' || c == 's' || c == 28) {
		matrix.setRange(0, 1, 1, 3);
		matrix.setRange(3, 1, 1, 3);
		matrix.setRange(6, 1, 1, 3);
		matrix.setRange(1, 0, 2, 1);
		matrix.setRange(4, 4, 2, 1);
		matrix.set(1, 4);
		matrix.set(5, 0);
	} else if (c == 'T' || c == 't' || c == 29) {
		matrix.setRange(0, 0, 1, 5);
		matrix.setRange(1, 2, 6, 1);
	} else if (c == 'U' || c == 'u' || c == 30) {
		matrix.setRange(0, 0, 6, 1);
		matrix.setRange(0, 4, 6, 1);
		matrix.setRange(6, 1, 1, 3);
	} else if (c == 'V' || c == 'v' || c == 31) {
		matrix.setRange(0, 0, 3, 1);
		matrix.setRange(0, 4, 3, 1);
		matrix.setRange(3, 1, 3, 1);
		matrix.setRange(3, 3, 3, 1);
		matrix.set(6, 2);
	} else if (c == 'W' || c == 'w' || c == 32) {
		matrix.setRange(0, 0, 6, 1);
		matrix.setRange(0, 4, 6, 1);
		matrix.setRange(6, 1, 1, 3);
		matrix.setRange(4, 2, 3, 1);
	} else if (c == 'X' || c == 'x' || c == 33) {
		matrix.set(0, 0);
		matrix.set(0, 4);
		matrix.set(3, 2);
		matrix.set(6, 0);
		matrix.set(6, 4);
		matrix.setRange(1, 1, 2, 1);
		matrix.setRange(1, 3, 2, 1);
		matrix.setRange(4, 1, 2, 1);
		matrix.setRange(4, 3, 2, 1);
	} else if (c == 'Y' || c == 'y' || c == 34) {
		matrix.set(0, 0);
		matrix.set(0, 4);
		matrix.set(1, 1);
		matrix.set(1, 3);
		matrix.setRange(2, 2, 5, 1);
	} else if (c == 'Z' || c == 'z' || c == 35) {
		matrix.setRange(0, 0, 1, 5);
		matrix.set(1, 0);
		matrix.set(1, 4);
		matrix.set(2, 3);
		matrix.set(3, 2);
		matrix.set(4, 1);
		matrix.set(5, 0);
		matrix.set(5, 4);
		matrix.setRange(6, 0, 1, 5);
	}
	return matrix;
}

Matrix::Matrix(byte rowLength, byte columnLength) {
	this->rowLength = rowLength;
	this->columnLength = columnLength;
	this->setAll(false);
}

Matrix::Matrix(byte length) {
	Matrix(length, length);
}

byte Matrix::getRowLength() {
	return this->rowLength;
}

byte Matrix::getColumnLength() {
	return this->columnLength;
}

bool Matrix::get(byte row, byte column) {
	if (row < this->getRowLength() && column < this->getColumnLength()) {
		return this->cells[row][column];
	} else {
		return false;
	}
}

void Matrix::set(byte row, byte column, bool enabled) {
	if (row < this->getRowLength() && column < this->getColumnLength()) {
		this->cells[row][column] = enabled;
	}
}

void Matrix::set(byte row, byte column) {
	this->set(row, column, !this->get(row, column));
}

void Matrix::setRow(byte row, bool enabled) {
	for (byte column = 0; column < this->getColumnLength(); column++) {
		this->set(row, column, enabled);
	}
}

void Matrix::setRow(byte row) {
	for (byte column = 0; column < this->getColumnLength(); column++) {
		this->set(row, column);
	}
}

void Matrix::setColumn(byte column, bool enabled) {
	for (byte row = 0; row < this->getRowLength(); row++) {
		this->set(row, column, enabled);
	}
}

void Matrix::setColumn(byte column) {
	for (byte row = 0; row < this->getRowLength(); row++) {
		this->set(row, column);
	}
}

void Matrix::setRange(byte rowStart, byte columnStart, byte rowLength, byte columnLength, bool enabled) {
	for (byte row = rowStart; row < rowStart + rowLength; row++) {
		for (byte column = columnStart; column < columnStart + columnLength; column++) {
			this->set(row, column, enabled);
		}
	}
}

void Matrix::setRange(byte rowStart, byte columnStart, byte rowLength, byte columnLength) {
	for (byte row = rowStart; row < rowStart + rowLength; row++) {
		for (byte column = columnStart; column < columnStart + columnLength; column++) {
			this->set(row, column);
		}
	}
}

void Matrix::setAll(bool enabled) {
	for (byte row = 0; row < this->getRowLength(); row++) {
		this->setRow(row, enabled);
	}
}

void Matrix::setAll() {
	for (byte row = 0; row < this->getRowLength(); row++) {
		this->setRow(row);
	}
}

void Matrix::flipHorizontal() {
	bool cells[this->getRowLength()][this->getColumnLength()];
	for (byte row = 0; row < this->getRowLength(); row++) {
		for (byte column = 0; column < this->getColumnLength(); column++) {
			cells[row][column] = this->get(row, column);
		}
		for (byte column = 0; column < this->getColumnLength(); column++) {
			this->set(row, this->getColumnLength() - column - 1, cells[row][column]);
		}
	}
}

void Matrix::flipVertical() {
	bool cells[this->getRowLength()][this->getColumnLength()];
	for (byte row = 0; row < this->getRowLength(); row++) {
		for (byte column = 0; column < this->getColumnLength(); column++) {
			cells[row][column] = this->get(row, column);
		}
	}
	for (byte row = 0; row < this->getRowLength(); row++) {
		for (byte column = 0; column < this->getColumnLength(); column++) {
			this->set(this->getRowLength() - row - 1, column, cells[row][column]);
		}
	}
}

void Matrix::rotate180() {
	bool cells[this->getRowLength()][this->getColumnLength()];
	for (byte row = 0; row < this->getRowLength(); row++) {
		for (byte column = 0; column < this->getColumnLength(); column++) {
			cells[row][column] = this->get(row, column);
		}
	}
	for (byte row = 0; row < this->getRowLength(); row++) {
		for (byte column = 0; column < this->getColumnLength(); column++) {
			this->set(this->getRowLength() - row - 1, this->getColumnLength() - column - 1, cells[row][column]);
		}
	}
}