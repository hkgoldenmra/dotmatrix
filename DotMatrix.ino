#include "DotMatrix.h"
#include "Matrix.h"

byte ROW_PINS[] = {13, 12, 11, 10, 9, 8, 7};
byte COLUMN_PINS[] = {5, 4, 3, 2, A0};
DotMatrix dm(ROW_PINS, COLUMN_PINS, sizeof(ROW_PINS) / sizeof(byte), sizeof(COLUMN_PINS) / sizeof(byte));

void setup() {
	dm.initial();
}

void loop() {
	Matrix matrix0 = Matrix::get7x5Template('1');
	dm.set(matrix0);
}