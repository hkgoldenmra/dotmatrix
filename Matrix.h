#ifndef MATRIX_H
#define MATRIX_H

#include <Arduino.h>

class Matrix {
	public:
		// class attributes
		const static byte MAX_ROW_LENGTH = 16;
		const static byte MAX_COLUMN_LENGTH = Matrix::MAX_ROW_LENGTH;

		// class methods
		static Matrix get8x8Template(char c);
		static Matrix get7x5Template(char c);

		// constructor
		Matrix(byte, byte);
		Matrix(byte);

		// object methods
		byte getRowLength();
		byte getColumnLength();
		bool get(byte, byte);
		void set(byte, byte, bool);
		void set(byte, byte);
		void setRow(byte, bool);
		void setRow(byte);
		void setColumn(byte, bool);
		void setColumn(byte);
		void setRange(byte, byte, byte, byte, bool);
		void setRange(byte, byte, byte, byte);
		void setAll(bool);
		void setAll();
		void flipHorizontal();
		void flipVertical();
		void rotate90();
		void rotate180();
		void rotate270();
	private:
		// object attributes
		byte rowLength;
		byte columnLength;
		bool cells[Matrix::MAX_ROW_LENGTH][Matrix::MAX_COLUMN_LENGTH];
};

#endif