#ifndef DOTMATRIX_H
#define DOTMATRIX_H

#include <Arduino.h>
#include "Matrix.h"

class DotMatrix {
	public:
		// class attribute
		static const byte MAX_ROW_LENGTH = 8;
		static const byte MAX_COLUMN_LENGTH = DotMatrix::MAX_ROW_LENGTH;

		// constructor
		DotMatrix(byte[], byte[], byte, byte);

		// object methods
		void initial();
		void setRow(byte, bool);
		void setColumn(byte, bool);
		void setAllRows(bool);
		void setAllColumns(bool);
		void setAll(bool);
		void set(Matrix, byte, byte, byte, byte, byte, byte);
		void set(Matrix, byte, byte, byte, byte);
		void set(Matrix, byte, byte);
		void set(Matrix);
		void moveUp(Matrix, byte, byte);
		void moveDown(Matrix, byte, byte);
		void moveLeft(Matrix, byte, byte);
		void moveRight(Matrix, byte, byte);
		void randomDot(byte);
	private:
		// object attribute
		byte rowPins[DotMatrix::MAX_ROW_LENGTH];
		byte columnPins[DotMatrix::MAX_COLUMN_LENGTH];
		byte rowLength;
		byte columnLength;
	};

#endif