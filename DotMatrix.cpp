#include "DotMatrix.h"
#include "Matrix.h"

DotMatrix::DotMatrix(byte rowPins[], byte columnPins[], byte rowLength, byte columnLength) {
	this->rowLength = rowLength;
	this->columnLength = columnLength;
	for (byte i = 0; i < this->rowLength; i++) {
		this->rowPins[i] = rowPins[i];
	}
	for (byte i = 0; i < this->columnLength; i++) {
		this->columnPins[i] = columnPins[i];
	}
}

void DotMatrix::initial() {
	for (byte row = 0; row < this->rowLength; row++) {
		pinMode(this->rowPins[row], OUTPUT);
		digitalWrite(this->rowPins[row], LOW);
	}
	for (byte column = 0; column < this->columnLength; column++) {
		pinMode(this->columnPins[column], OUTPUT);
		digitalWrite(this->columnPins[column], HIGH);
	}
}

void DotMatrix::setRow(byte row, bool enabled) {
	if (row < this->rowLength) {
		digitalWrite(this->rowPins[row], ((enabled) ? HIGH : LOW));
	}
}

void DotMatrix::setColumn(byte column, bool enabled) {
	if (column < this->columnLength) {
		digitalWrite(this->columnPins[column], ((enabled) ? LOW : HIGH));
	}
}

void DotMatrix::setAllRows(bool enabled) {
	for (byte row = 0; row < this->rowLength; row++) {
		this->setRow(row, enabled);
	}
}

void DotMatrix::setAllColumns(bool enabled) {
	for (byte column = 0; column < this->columnLength; column++) {
		this->setColumn(column, enabled);
	}
}

void DotMatrix::setAll(bool enabled) {
	this->setAllRows(enabled);
	this->setAllColumns(enabled);
}

void DotMatrix::set(Matrix matrix, byte rowStart, byte columnStart, byte rowLength, byte columnLength, byte rowDestination, byte columnDestination) {
	byte r = rowDestination;
	for (int row = rowStart; row < rowStart + rowLength; row++) {
		this->setRow(r, true);
		byte c = columnDestination;
		for (int column = columnStart; column < columnStart + columnLength; column++) {
			this->setColumn(c, matrix.get(row, column));
			c++;
		}
		delay(1);
		this->setRow(r, false);
		this->setAllColumns(false);
		r++;
	}
}

void DotMatrix::set(Matrix matrix, byte rowStart, byte columnStart, byte rowLength, byte columnLength) {
	this->set(matrix, rowStart, columnStart, rowLength, columnLength, 0, 0);
}

void DotMatrix::set(Matrix matrix, byte rowLength, byte columnLength) {
	this->set(matrix, 0, 0, rowLength, columnLength);
}

void DotMatrix::set(Matrix matrix) {
	this->set(matrix, this->rowLength, this->columnLength);
}

void DotMatrix::moveUp(Matrix matrix, byte length, byte steps) {
	for (int i = -this->rowLength; i < length; i++) {
		for (byte t = 0; t < steps; t++) {
			for (int row = 0; row < this->rowLength; row++) {
				int r = row - i;
				if (row < length) {
					this->setRow(r, true);
				}
				for (int column = 0; column < this->columnLength; column++) {
					this->setColumn(column, matrix.get(row, column));
				}
				delay(1);
				if (row < length) {
					this->setRow(r, false);
				}
				this->setAllColumns(false);
			}
		}
	}
}

void DotMatrix::moveDown(Matrix matrix, byte length, byte steps) {
	for (int i = -length; i < this->rowLength; i++) {
		for (byte t = 0; t < steps; t++) {
			for (int row = 0; row < this->rowLength; row++) {
				int r = row + i;
				if (row < length) {
					this->setRow(r, true);
				}
				for (int column = 0; column < this->columnLength; column++) {
					this->setColumn(column, matrix.get(row, column));
				}
				delay(1);
				if (row < length) {
					this->setRow(r, false);
				}
				this->setAllColumns(false);
			}
		}
	}
}

void DotMatrix::moveLeft(Matrix matrix, byte length, byte steps) {
	for (int i = -this->columnLength; i < length; i++) {
		for (byte t = 0; t < steps; t++) {
			for (int row = 0; row < this->rowLength; row++) {
				this->setRow(row, true);
				for (int column = 0; column < length; column++) {
					int c = column - i;
					this->setColumn(c, matrix.get(row, column));
				}
				delay(1);
				this->setRow(row, false);
				this->setAllColumns(false);
			}
		}
	}
}

void DotMatrix::moveRight(Matrix matrix, byte length, byte steps) {
	for (int i = -length; i < this->columnLength; i++) {
		for (byte t = 0; t < steps; t++) {
			for (int row = 0; row < this->rowLength; row++) {
				this->setRow(row, true);
				for (int column = 0; column < length; column++) {
					int c = column + i;
					this->setColumn(c, matrix.get(row, column));
				}
				delay(1);
				this->setRow(row, false);
				this->setAllColumns(false);
			}
		}
	}
}

void DotMatrix::randomDot(byte steps) {
	byte row = rand() % this->rowLength;
	byte column = rand() % this->columnLength;
	for (unsigned int t = 0; t < steps; t++) {
		this->setRow(row, true);
		this->setColumn(column, true);
		delay(1);
	}
	this->setRow(row, false);
	this->setColumn(column, false);
}